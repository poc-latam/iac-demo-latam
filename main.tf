resource "google_bigquery_dataset" "pokedex" {
  dataset_id                  = var.dataset_id
  friendly_name               = var.friendly_name
  description                 = var.description
  location                    = var.location
  delete_contents_on_destroy  = true

  labels = {
    project = "demo-latam"
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_bigquery_table" "pocket_monsters" {
  dataset_id          = google_bigquery_dataset.pokedex.dataset_id
  table_id            = var.table_id
  deletion_protection = true

  schema = var.schema
}

resource "google_pubsub_schema" "pokemon_schema" {
  name       = var.pubsub_schema_name
  type       = "AVRO"
  definition = "{\"type\":\"record\",\"name\":\"Pokemon\",\"fields\":[{\"name\":\"name\",\"type\":\"string\"},{\"name\":\"type\",\"type\":\"string\"}]}"
}

resource "google_pubsub_topic" "topic_pokemon" {
  name       = var.pubsub_topic_name
  depends_on = [google_pubsub_schema.pokemon_schema]

  schema_settings {
    schema   = "projects/aa-study/schemas/${var.pubsub_schema_name}"
    encoding = "JSON"
  }
}

resource "google_pubsub_subscription" "pokemon_pull_subscription" {
  name       = var.pubsub_subscription_name
  depends_on = [google_pubsub_topic.topic_pokemon]
  topic      = "projects/aa-study/topics/${var.pubsub_topic_name}"

  labels = {
    project = "demo-latam"
  }

  message_retention_duration = "1200s"
  retain_acked_messages      = true
  ack_deadline_seconds       = 20

  expiration_policy {
    ttl = "300000.5s"
  }
  retry_policy {
    minimum_backoff = "10s"
  }

  enable_message_ordering = false
}

resource "google_pubsub_subscription" "pokemon_to_bigquery_subscription" {
  name       = var.pubsub_to_bigquery_subscription_name
  depends_on = [google_pubsub_topic.topic_pokemon]
  topic      = "projects/aa-study/topics/${var.pubsub_topic_name}"

  bigquery_config {
    table            = "${google_bigquery_table.pocket_monsters.project}.${google_bigquery_table.pocket_monsters.dataset_id}.${google_bigquery_table.pocket_monsters.table_id}"
    use_topic_schema = true
  }

  labels = {
    project = "demo-latam"
  }

  message_retention_duration = "1200s"
  retain_acked_messages      = true
  ack_deadline_seconds       = 20

  expiration_policy {
    ttl = "300000.5s"
  }
  retry_policy {
    minimum_backoff = "10s"
  }

  enable_message_ordering = false
}
