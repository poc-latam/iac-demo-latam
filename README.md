# iac-demo-latam

Esta implementación de IAC contiene el código fuente para el despliegue de la siguiente infraetructura haciendo uso de TERRAFORM: 


La documentación de esta parte de la solución se puede ver a más detalle en el primer apartado **"Parte 1:Infraestructura e IaC"** del siguiente documento: https://docs.google.com/document/d/1htJH7I6k_-K5hBuSotXtzShUxCOwhL5yNnaA9ioAvjs/edit?usp=sharing


## Contenido del repositorio

Este repositorio contiene los siguientes archivos:
- **main.tf** : Este archivo contiene la configuración principal, desde allí se definieron los módulos utilizados para el despliegue de la infraestructura inicial, estos módulos son:
  - Módulo BigQuery: Por medio de este módulo se configuró la base de datos que se utilizara, destacando dos componentes el dataset y la tabla que recibirá la ingesta de datos.
  - Módulo Pub/Sub: Este componente contiene la configuración necesaria para la creación del tópico que publicará los mensajes, 2 suscripciones, la primera es de tipo pull para traer los mensajes que son publicados directamente sobre el tópico, la segunda es de tipo push y se conecta directamente con el dataset para publicar el cuerpo del mensaje en una tabla de bigquery y finalmente un esquema que representa el cuerpo de mensaje que se puede cargar sobre el tópico.
- **vars.tf**: En este archivo se añadieron las variables que son llamadas desde el main para la creación de infraestructura, se agregan los nombres de los componentes que harán parte de la solución.
- **provider.tf**: En este archivo se definió el provider que se encargará de cargar la configuración de los módulos en el main, para este caso es la nube de google.
- **output.tf**: En este archivo se definieron los outputs de salida, para ver los recursos creados.

## Creando la infraestructura a partir del repo

1. Se debe clonar el repositorio.
2. Se debe modificar en el archivo "provider.tf" los campos:
- Credentials: Acá se debe cargar la KEY de una SERVICE-ACCOUNT de google con los permisos necsarios para la creación de los recursos.
- Project: Acá se debe reemplazar este campo con el campo respectivo del proyecto en el cuál se desplegara la infraestructura.
- Region: Acá se debe especificar la región en la cuál se desplegara la infraestructura.
3. Se deben ejecutar los siguientes comandos:
   1. terraform init: Este comando iniciara el provider y cargará los módulos necesarios para el despliegue de la infraestructura.
   2. terraform validate: Este comando validará que la infraestructura esté bien configurada, en caso de tener algún fallo, lo reportara en la salida posterior.
   3. terraform apply –auto-approve: Este comando se encargará de aplicar la configuración realizada sobre los archivos .tf para la creación de la infraestructura a partir de código.

## Eliminar la infraestructura creada
En caso de querer eliminar la infraestructura creada, se debe ejecutar el siguiente comando:
terraform destroy --auto-approve