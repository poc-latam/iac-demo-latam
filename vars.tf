variable "dataset_id" {
  description = "ID for the BigQuery dataset"
  default     = "pokedex"
}

variable "friendly_name" {
  description = "Friendly name for the BigQuery dataset"
  default     = "Pokedex Dataset"
}

variable "description" {
  description = "Description for the BigQuery dataset"
  default     = "Dataset for Pokemon data"
}

variable "location" {
  description = "Location for the BigQuery dataset"
  default     = "us-central1"
}

variable "table_id" {
  description = "ID for the BigQuery table"
  default     = "pocket-monsters"
}

variable "schema" {
  description = "Schema for the BigQuery table"
  default     = <<EOF
[
  {
    "name": "name",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "The name of the pokemon"
  },
  {
    "name": "type",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "The type of the pokemon"
  }
]
EOF
}

variable "pubsub_schema_name" {
  description = "Name for the Pub/Sub schema"
  default     = "pokemon-schema"
}

variable "pubsub_topic_name" {
  description = "Name for the Pub/Sub topic"
  default     = "pokemon-topic"
}

variable "pubsub_subscription_name" {
  description = "Name for the Pub/Sub subscription"
  default     = "pokemon-pull-subscription"
}

variable "pubsub_to_bigquery_subscription_name" {
  description = "Name for the Pub/Sub to BigQuery subscription"
  default     = "pokemon-to-bigquery-subscription"
}
