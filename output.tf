output "dataset_id" {
  value = google_bigquery_dataset.pokedex.dataset_id
}

output "table_id" {
  value = google_bigquery_table.pocket_monsters.table_id
}

output "pubsub_schema_name" {
  value = google_pubsub_schema.pokemon_schema.name
}

output "pubsub_topic_name" {
  value = google_pubsub_topic.topic_pokemon.name
}

output "pull_subscription_name" {
  value = google_pubsub_subscription.pokemon_pull_subscription.name
}

output "to_bigquery_subscription_name" {
  value = google_pubsub_subscription.pokemon_to_bigquery_subscription.name
}
